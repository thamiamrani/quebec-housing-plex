import abc
from typing import List

from ..realtors.house_result import HouseResults


class Notifier(abc.ABC):
    @abc.abstractmethod
    def send(self, messages: List[HouseResults]) -> None:
        pass
