import sys

from .deps import get_agent, get_app_config, get_notifier


def exit_with_error_message() -> None:
    print(
        "\nError, program syntax:\n\tpython -m app.main config_file_path.yml <email|print>\n"
    )
    sys.exit(-1)


def main():
    if len(sys.argv) != 3:
        exit_with_error_message()

    _, config_file, mode = sys.argv

    app_config = get_app_config(config_file)
    notifier = get_notifier(mode, app_config)

    results = get_agent(app_config).search()
    notifier.send(results)


if __name__ == "__main__":
    main()
