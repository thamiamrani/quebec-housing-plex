from typing import List, Optional

from pydantic import BaseModel


class HouseResult(BaseModel):
    description: str
    link: str
    price: Optional[int]
    image_link: str
    revenue: Optional[str]
    plex_title: Optional[str]

    @classmethod
    def error(cls) -> "HouseResult":
        return HouseResult(description="Something failed :(", link="Sorry")

    @classmethod
    def new_house(
        cls, link: str, price: Optional[int], image_link: str, revenue: str="", plex_title: str=""
    ) -> "HouseResult":
        return HouseResult(
            description="Nouvelle propriété",
            link=link,
            price=price,
            image_link=image_link,
            revenue=revenue,
            plex_title=plex_title
        )

    @classmethod
    def new_price(
        cls, link: str, price: Optional[int], image_link: str, revenue: str="", plex_title: str=""
    ) -> "HouseResult":
        return HouseResult(
            description="Nouveau prix", link=link, price=price, image_link=image_link, revenue=revenue, plex_title=plex_title
        )

    def price_at_most(self, number: int) -> bool:
        if self.price is None:
            return False

        return self.price <= number

    def price_as_str(self) -> str:
        if self.price is None:
            return "?"

        return "{:,} $".format(self.price).replace(",", " ")
    
    def potential_revenue(self):
        if self.__revenue_as_int():
            return f'{round((self.__revenue_as_int()/12)*100/self.price, 2)} %'
        return "?"
    
    def monthly_revenue(self):
        if self.__revenue_as_int():
            return f'{round((self.__revenue_as_int()/12))} $ / mois'
        return "?"
    
    def __revenue_as_int(self):
        return int(self.revenue[18:-2].replace(" ", ""))



class HouseResults(BaseModel):
    meta: str
    houses: List[HouseResult]
