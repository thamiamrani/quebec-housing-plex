from functools import lru_cache
from typing import List

import yaml
from selenium import webdriver
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.remote.webdriver import WebDriver
from webdriver_manager.chrome import ChromeDriverManager  # type: ignore

from .config import AppConfig, HotmailSettings
from .notifications.console_notifier import ConsoleNotifier
from .notifications.hotmail_notifier import HotmailNotifier
from .notifications.notifier import Notifier
from .realtors.bot_agent import BotAgent
from .realtors.centris_realtor import CentrisRealtor
from .realtors.centris_plex_realtor import CentrisPlexRealtor
from .realtors.du_proprio_realtor import DuProprioRealtor
from .realtors.realtor import Realtor

USER_AGENT = (
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 "
    "(KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36"
)
MAX_PRICE = 999_999_999


def get_app_config(config_path: str) -> AppConfig:
    with open(config_path, "r") as stream:
        data = yaml.safe_load(stream)

    return AppConfig.parse_obj(data)


@lru_cache()
def _driver_manager() -> str:
    return ChromeDriverManager().install()


def _driver() -> WebDriver:
    options = ChromeOptions()
    options.add_argument("--headless")
    options.add_argument(
        f"--user-agent={USER_AGENT}"
    )  # needed so centris do not stop in headless mode
    options.add_argument("--no-sandbox")  # needed for docker
    options.add_argument("--disable-setuid-sandbox")  # needed for docker
    svc = ChromeService(_driver_manager())

    return webdriver.Chrome(service=svc, options=options)


def _realtors(app_config: AppConfig) -> List[Realtor]:
    centris: List[Realtor] = [
        CentrisRealtor(_driver, c.meta, c.url, c.max_price or MAX_PRICE)
        for c in app_config.centris
    ]
    du_proprio: List[Realtor] = [
        DuProprioRealtor(_driver, dp.meta, dp.url) for dp in app_config.du_proprio
    ]
    centris_plex: List[Realtor] = [
        CentrisPlexRealtor(_driver, cp.meta, cp.url, cp.max_price or MAX_PRICE)
        for cp in app_config.centris_plex
    ]
    return centris_plex + centris + du_proprio


def get_agent(app_config: AppConfig) -> BotAgent:
    return BotAgent(_realtors(app_config))


def _console_notifier() -> Notifier:
    return ConsoleNotifier()


def _hotmail_notifier(app_config: AppConfig) -> Notifier:
    settings = HotmailSettings()
    return HotmailNotifier(
        settings.hotmail_user, settings.hotmail_password, app_config.recipients
    )


def get_notifier(mode: str, app_config: AppConfig) -> Notifier:
    return _console_notifier() if mode == "print" else _hotmail_notifier(app_config)
